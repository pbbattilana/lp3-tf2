Implementa un programa que genere un vector de MAX numeros enteros aleatorios
comprendidos entre 0 y 9. A continuación buscará secuencialmente en dicho vector el
número que se le pasa como primer argumento al programa. El programa debe crear
tantos hilos como se le indique en el segundo argumento. Y cada hilo tiene que realizar la
búsqueda en un fragmento consecutivo del vector de un tamaño aproximadamente igual
para todos los hilos. Una vez que un hilo encuentre el número, todos los hilos finalizan la
búsqueda. El hilo principal esperara que acaben todos los hilos y mostrará por pantalla el
índice de la posición en la que esté el número con el número de proceso, o el mensaje deque no está en el vector.
$ buscar 4 5
el programa buscará en el vector el número 4 con 5 hilos ejecutándose concurrentemente.
Ejemplo, para este vector de 20 elementos con 5 hilos de búsqueda
| 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12 | 13 | 14 | 15 | 16 | 17 | 18 | 19 |
| -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- |
|  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |

el primer primer hilo hace la búsqueda en el sub vector de las posiciones [0,3], el segundo
hilo en el subvector [4,7], y así sucesivamente.

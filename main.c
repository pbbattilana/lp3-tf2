#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <math.h>
#include <unistd.h>
#include <time.h>

int supreme[99];
static int initial_value;
void *thread_routine(void *arg)
{
	int numBuscado = *((int*)arg+0);
	int i = *((int*)arg+1);
	int pos = -1;
	for (int j = i-initial_value; j < i; j++)
	{
		if(numBuscado == supreme[j])
			pos = j;
	}
	if(pos == -1){
		printf("\nEn el hilo con id = %ld, no se encontraron coincidencias", pthread_self());
		pthread_exit(NULL);
	}else{
		printf("\nEn el hilo con id = %ld, se encontró coincidencia en la pos = %d", pthread_self(), pos);
	}
}

int main(int argc, char *argv[])
{
	pthread_t thread1;
	srand(time(NULL));

	char *string = argv[1];
	int vec[3], posicion = -1;
	int i = 0;

	char *token;
	token = strtok(string, ",");
	while (token != 0)
	{
		*(vec + i) = atoi(token);
		token = strtok(0, ",");
		i++;
	}
	int tamVector = vec[0] * vec[1];
    printf ("\nEl array generado es de longitud %d \n", tamVector);
	printf("\nEl numero que buscas es: %d con %d hilos\n",vec[0],vec[1]);
	for(i=0;i<tamVector;i++)
    {
        supreme[i]=rand()%10;
    }

	printf(" ");
    for(i=0;i<tamVector;i++)        
    {
        printf("___");
    }
    
    printf("\n|");
    for(i=0;i<tamVector;i++)
    {
        if(i<10){
        printf(" %d|", i);
        }else 
        printf("%d|", i);
    }
    printf("\n ");
    for(i=0;i<tamVector;i++)        
    {
        printf("¯¯¯");
    }

    printf("\n|");
    for(i=0;i<tamVector;i++)
    {
        printf(" %d|", supreme[i]);
    }
    printf("\n ");
    for(i=0;i<tamVector;i++)        
    {
        printf("¯¯¯");
    }

	int value[2];
	value[0] = vec[0]; initial_value = vec[0];
	for (i = vec[0]; i < tamVector+vec[0]; i=i+vec[0])
	{
		value[1] = i;
		if (0 != pthread_create(&thread1, NULL, thread_routine, &value))
		return -1;
		pthread_join(thread1, NULL);
	}

	printf("\n");
	return 0;

}
